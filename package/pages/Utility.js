/**
 * Created by Leon on 28.7.2016..
 */

var Point = function (x, y) {
    var point = {
        x : x,
        y : y
    };

    return point;
};

var Circle = function (x, y, radius) {
    var circle = {
        x : x,
        y : y,
        radius : radius,

        containsPoint : function (x, y) {
            var distance = Math.sqrt(Math.pow(x - this.x, 2) + Math.pow(y - this.y, 2));
            return distance <= this.radius;
        },
    };

    return circle;
};

var Rectangle = function (x, y, width, height) {
    var rectangle = {
        x : x,
        y : y,
        width : width,
        height : height,

        containsPoint : function (x, y) {
            if (x < this.x) return false;
            if (x > (this.x + this.width)) return false;
            if (y < this.y) return false;
            if (y > this.y + this.height) return false;

            return true;
        },

        draw : function(context, strokeWidth) {
            var oldWidth = context.lineWidth;
            context.lineWidth = strokeWidth;
            context.strokeRect(x, y, width, height);
            context.lineWidth = oldWidth;
        }
    };

    return rectangle;
};

function getMousePos(canvas, evt) {
    var rect = canvas.getBoundingClientRect();
    return {
        x: evt.clientX - rect.left,
        y: evt.clientY - rect.top
    };
}