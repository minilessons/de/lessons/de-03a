var Gate = function(startA, startB, operation, gateSymbol) {

    var gate = {

        gateSymbol : undefined,

        getInputA : function () {
            return this.inputA;
        },

        getInputB : function () {
            return this.inputB;
        },

        getOutput : function() {
            return this.output;
        },

        setInputA: function(a) {
            this.inputA = a;
            this.output = this.operation(this.inputA, this.inputB);
        },

        setInputB: function(b) {
            this.inputB = b;
            this.output = this.operation(this.inputA, this.inputB);
        },

        operation : undefined,

        draw: function (context, x, y, width, height) {
            context.clearRect(x, y, width, height);

            var oldLength = context.lineWidth;

            context.lineWidth = 3;
            context.strokeRect(x, y, width, height);

            var y1 = y + height / 4; // Y coordinate of input A
            var y2 = y + 3 * height / 4; // Y coordinate of input B
            var yOut = y + height / 2; // Y coordinate of the output

            if (this.gateSymbol != undefined) {
                // a rough approximation, just so the text scales a little bit at least
                var fontSize = Math.min(width, height) / 3;

                var oldFont = context.font;
                context.font = fontSize + "px Arial";

                var textWidth = context.measureText(this.gateSymbol).width;
                var xOffset = x + width / 2 - textWidth / 2;
                var yOffset = y + height / 2 + fontSize / 3;
                context.fillText(this.gateSymbol, xOffset, yOffset);

                context.font = oldFont;
            }

            this.inputAConnector = new Point(x, y1);
            this.inputBConnector = new Point(x, y2);
            this.outputConnector = new Point(x + width, yOut);

            context.lineWidth = oldLength;
        }
    };

    gate.inputA = startA;
    gate.inputB = startB;
    gate.operation = operation;
    gate.output = gate.operation(startA, startB);

    gate.gateSymbol = gateSymbol;

    return gate;
};